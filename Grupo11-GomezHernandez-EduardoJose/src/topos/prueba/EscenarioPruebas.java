package topos.prueba;

import java.util.LinkedList;

import topos.elementos.Elemento;
import topos.elementos.TopoTorpe;
import topos.estructura.Direccion;
import topos.estructura.Escenario;
import topos.estructura.PanelBasico;
import topos.estructura.Posicion;

public class EscenarioPruebas {
	public static void main (String args[]) {
		Escenario escenario1 = new Escenario(5, 5);
		
		System.out.println(escenario1.getAncho());
		System.out.println(escenario1.getAlto());
		
		Posicion posicion1 = new Posicion(1,2);
		System.out.println(escenario1.isValidPosition(posicion1));
		
		posicion1.desplazar(4, -1);
		System.out.println(escenario1.isValidPosition(posicion1));
		
		PanelBasico panelBasico1 = new PanelBasico(2,3,false);
		posicion1.setX(2);
		posicion1.setY(3);
		escenario1.setPanel(panelBasico1);
		System.out.println(escenario1.isVisible(posicion1));
		
		escenario1.desplazarObjetivo(Direccion.DERECHA);
		System.out.println(escenario1.getObjetivo().getX());
		System.out.println(escenario1.getObjetivo().getY());
		
		System.out.println("PRUEBA TOPOS");
		Elemento a = new TopoTorpe(1,2);
		Elemento b = new TopoTorpe(2,3);
		
		//System.out.println(escenario1.addTopos(a,b).size());
		LinkedList<Elemento> rechazados = escenario1.addElementos(a,b);
		if(rechazados.isEmpty()) System.out.println("Vacio");
		
		Posicion c = new Posicion(1,2);
		if(escenario1.getElemento(c) == a) System.out.println("topo 1 dento");
		
		Elemento d = new TopoTorpe(2,3);
		LinkedList<Elemento> rechazado = escenario1.addElementos(d);
		if(!rechazado.isEmpty()) System.out.println("Topo ya dentro"); 
		
	}
}
