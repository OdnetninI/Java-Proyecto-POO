package topos.prueba;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import topos.elementos.Elemento;
import topos.elementos.TopoCiego;
import topos.estructura.Escenario;
import topos.estructura.Posicion;

public class PruebaColecciones {
	public static void main (String[] args) {
		Set<Posicion> posiciones = new HashSet<Posicion>();
		Posicion p1 = new Posicion(0,0);
		Posicion p2 = new Posicion(1,2);
		Posicion p3 = new Posicion(0,0);
		posiciones.add(p1);
		posiciones.add(p2);
		posiciones.add(p3);
		
		for (Posicion p : posiciones) {
			System.out.println(p);
		}
		
		Escenario escene = new Escenario(12, 12);
		Elemento a = new TopoCiego(5,5);
		escene.addElementos(a);
		
		Collection<Elemento> elem = escene.getElementos();
		
		elem.add(a);
	}
}
