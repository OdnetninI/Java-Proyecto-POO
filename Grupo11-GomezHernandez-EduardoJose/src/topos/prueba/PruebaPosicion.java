package topos.prueba;

import topos.estructura.Posicion;

public class PruebaPosicion {
	public static void main(String[] args) {
		Posicion a = new Posicion();
		Posicion b = new Posicion(1, 3);
		Posicion c = new Posicion(b);
		Posicion d = a;
		
		
		a.desplazar(5, 9);
		a.desplazar(1, 1);
		
		System.out.println("La posicion del objeto a es: " + a.getX() + ", " + a.getY());
		System.out.println("La posicion del objeto b es: " + b.getX() + ", " + b.getY());
		System.out.println("La posicion del objeto c es: " + c.getX() + ", " + c.getY());
		System.out.println("La posicion del objeto d es: " + d.getX() + ", " + d.getY());
		
		a.desplazar(1, 1);
		System.out.println("La posicion del objeto a es: " + a.getX() + ", " + a.getY());
		System.out.println("La posicion del objeto d es: " + d.getX() + ", " + d.getY());
		
		d.desplazar(1, 1);
		System.out.println("La posicion del objeto a es: " + a.getX() + ", " + a.getY());
		System.out.println("La posicion del objeto d es: " + d.getX() + ", " + d.getY());
	}
}
