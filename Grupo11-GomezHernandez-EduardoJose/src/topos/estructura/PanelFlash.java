package topos.estructura;

/**
 * Representa el tipo de panel flash en el juego.
 * Un panel flash, es aquel, que regresa al estado original
 * en un lapso de tiempo infimo.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class PanelFlash extends PanelBasico {
	
	private int numeroAct = 0;
	
	/**
	 * Constructor
	 * 
	 * @param x Posicion x en el tablero
	 * @param y Posicion y en el tablero
	 * @param visible estado inicial de visibilidad
	 */
	public PanelFlash(int x, int y, boolean visible) {
		super(x,y,visible);
	}
	
	/**
	 * Constructor
	 * 
	 * @param x Posicion x en el tablero
	 * @param y Posicion y en el tablero
	 */
	public PanelFlash(int x, int y) {
		super(x,y);
	}
	
	public PanelFlash (PanelFlash copy) {
		this(copy.getPosicion().getX(), copy.getPosicion().getY(), copy.getVisible());
	}
	
	/**
	 * Refinamiento
	 * Hacemos visible el panel, solo cuando se llame a
	 * actualizar el panel 2 veces.
	 * 
	 */
	@Override
	public void actualizar() {
		if (this.getVisible() == false) {
			this.numeroAct++;
			if (this.numeroAct >= 2) {
				this.numeroAct = 0;
				this.setVisible(true);
			}
		}
	}
	
	/**
	 * Redefinición
	 * Obtenemos la imagen asociada al panel
	 * 
	 */
	@Override
	public String getRuta() {
		return "imagenes/panel-flash.gif";
	}
	
	@Override
	public PanelFlash clone() {
		return new PanelFlash(this);
	}
	
}