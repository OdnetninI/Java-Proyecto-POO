package topos.estructura;

import java.util.Random;

/**
 * Enumerado de las posibles direcciones.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public enum Direccion {
	ARRIBA,ABAJO,DERECHA,IZQUIERDA;
	
	/**
	 * Obtiene una de las direeciones de forma aleatoria
	 * 
	 */
	public static Direccion getRandom() {
		Random rand = new Random();
		return Direccion.values()[rand.nextInt(Direccion.values().length)];
	}
	
}
