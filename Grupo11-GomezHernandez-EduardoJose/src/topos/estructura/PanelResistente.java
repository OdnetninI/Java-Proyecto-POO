package topos.estructura;

import java.util.LinkedList;

/**
 * Representa el tipo de panel resistente en el juego.
 * Un panel resistente requiere ser golpeado un numero
 * determinado de veces en menos de 1 segundo para ser derribado.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class PanelResistente extends PanelBasico {

	private final int dureza;
	private static final int UN_SEGUNDO_EN_MILISEGUNDOS = 1000; 
	private LinkedList<Long> listaDeGolpes = new LinkedList<Long>();
	
	/**
	 * Constructor
	 * 
	 * @param x Posicion x en el tablero
	 * @param y Posicion y en el tablero
	 * @param dureza cantidad de golpes necesarios en menos de 1 segundo para ser derribado
	 * @param visible estado inicial de visibilidad
	 */
	public PanelResistente(int x, int y, int dureza, boolean visible) {
		super(x, y, visible);
		this.dureza = dureza;
	}
	
	/**
	 * Constructor
	 * 
	 * @param x Posicion x en el tablero
	 * @param y Posicion y en el tablero
	 * @param dureza cantidad de golpes necesarios en menos de 1 segundo para ser derribado
	 */
	public PanelResistente(int x, int y, int dureza) {
		super(x, y);
		this.dureza = dureza;
	}
	
	public PanelResistente (PanelResistente copy) {
		this(copy.getPosicion().getX(), copy.getPosicion().getY(), copy.getDureza(), copy.getVisible());
	}
	
	/**
	 * Refinamiento
	 * Solo golpeamos el panel si recibe una cantidad de golpes
	 * determinada por la dureza, en menos de 1 segundo
	 * 
	 */
	@Override
	public void golpear() {
		this.listaDeGolpes.addLast(System.currentTimeMillis());
		if(this.listaDeGolpes.size() == this.dureza){
			if((this.listaDeGolpes.get(this.dureza - 1) - this.listaDeGolpes.get(0)) <= UN_SEGUNDO_EN_MILISEGUNDOS) {
				super.golpear();
				this.listaDeGolpes.clear();
			}
			else {
				this.listaDeGolpes.remove(0);
			}
		}
	}
	
	/**
	 * Obtener la imagen asociada al panel
	 * 
	 */
	@Override
	public String getRuta() {
		return "imagenes/panel-resistente.gif";
	}
	
	public int getDureza() {
		return this.dureza;
	}
	
	@Override
	public PanelResistente clone() {
		return new PanelResistente(this);
	}
	
}
