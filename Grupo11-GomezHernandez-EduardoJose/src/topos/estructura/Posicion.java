package topos.estructura;

/**
 * Representa la posicion en coordenadas. 
 * 
 * @author Eduardo José y Diego Alonso
 *
 */
public class Posicion implements Cloneable {
	private int x;
	private int y;
	
	/**
	 * Constructor por defecto.
	 */
	public Posicion() {
		this(0,0);
	}
	
	/**
	 * Define una posición a partir de las coordenadas x e y.
	 * 
	 * @param x coordenada x de la posicion.
	 * @param y coordenada y de la posicion.
	 */
	public Posicion(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Define una posicion a partir de otra dada.
	 * 
	 * @param copy posicion a copiar.
	 */
	public Posicion(Posicion copy) {
		this(copy.x, copy.y);
	}
	
	/**
	 * Metodo de consulta de la coordenada x.
	 * 
	 * @return Devuelve la coordenada x de la posicion.
	 */
	public int getX() {return this.x;}
	
	/**
	 * Metodo de consulta de la coordenada y.
	 * 
	 * @return Devuelve la coordenada y de la posicion.
	 */
	public int getY() {return this.y;}
	
	/**
	 * Metodo para establecer la coordenada x de la posicion.
	 * 
	 * @param x coordenada x de la posicion.
	 */
	public void setX(int x) {this.x = x;}
	
	/**
	 * Metodo para establecer la coordenada y de la posicion.
	 * 
	 * @param y coordenada y de la posicion.
	 */
	public void setY(int y) {this.y = y;}
	
	/**
	 * Desplaza la posicion n unidades indicadas en cada parametro.
	 * 
	 * @param x numero de unidades que se desplaza la coordenada x. 
	 * @param y numero de unidades que se desplaza la coordenada y.
	 */
	public void desplazar (int x, int y) {
		this.x += x;
		this.y += y;
	}
	
	/**
	 * Desplazar de posicion en una direccion una unidad.
	 * 
	 * @param direccion La direccion del movimiento, que no sea nula.
	 */
	public void desplazar (Direccion direccion){
		if(direccion == Direccion.ARRIBA) this.desplazar(0, 1);
		else if(direccion == Direccion.ABAJO) this.desplazar(0, -1);
		else if(direccion == Direccion.IZQUIERDA) this.desplazar(-1, 0);
		else if(direccion == Direccion.DERECHA) this.desplazar(1, 0);
	}
	
	/**
	 * Metodo para obtener la posicion vecina deseada.
	 * 
	 * @param direccion La direccion del movimiento, que no sea nula.
	 * @return Una nueva posicion vecina a la que hemos indicado.
	 */
	public Posicion getVecina (Direccion direccion){
		Posicion vecina = new Posicion(this);
		vecina.desplazar(direccion);
		return vecina;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new Posicion(this.x, this.y);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		else if (this.hashCode() == obj.hashCode()) return true;
		else return false;
	}
	
	@Override
	public int hashCode() {
		return x*y*x + y*31;
	}
	
	@Override
	public String toString() {
		return this.getClass().getName() + " Posicion X: " + Integer.toString(this.x) + " Posicion Y: " + Integer.toString(this.y);
	}
	
}
