package topos.estructura;

import java.awt.Color;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import topos.elementos.Elemento;
import topos.juego.GestionJuego;
import topos.vista1.Alarma;
import topos.vista1.Pantalla;

/**
 * Representa el escenario del juego.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class Escenario {
	private final int ancho;
	private final int alto;
	private PanelBasico[][] paneles;
	private Posicion objetivo;
	private Map<Posicion, Elemento> elementos;
	private GestionJuego partida;
	private Pantalla pantalla;
	private static final int ZOMBIE = 200;
	private boolean visibles = true;
	private int puntosTotales = 0;
	private boolean run = false;
	
	/**
	 * Constructor
	 * 
	 * @param ancho ancho del escenario
	 * @param alto alto del escenario
	 */
	public Escenario(int ancho, int alto) {
		if (ancho <= 0 || alto <= 0) throw new IllegalArgumentException("La anchura y la altura del escenario deben ser positivas y mayores que 0.");
		this.ancho = ancho;
		this.alto = alto;
		this.objetivo = new Posicion();
		this.paneles = new PanelBasico[this.ancho][this.alto];
		this.elementos = new HashMap<Posicion, Elemento>();
		this.partida = null;
		this.pantalla = null;
		
		for(int j = 0; j < this.alto; j++)  {
			for(int i = 0; i < this.ancho; i++) {
				this.paneles[i][j] = new PanelBasico(i, j);
			}
		}
	}
	
	/**
	 * Actualiza la pantalla
	 * 
	 */
	public void refrescarPantalla() {
		this.pantalla.resetear();
		for(int j = 0; j < this.alto; j++)  {
			for(int i = 0; i < this.ancho; i++) {
				if(this.paneles[i][j].getVisible() && this.visibles) {
					this.pantalla.addImagen(i, j, this.paneles[i][j].getRuta());
				}
			}
		}
		
		for (Elemento elemento : this.elementos.values()) {
			if (this.isVisible(elemento.getPosicion()) || !this.visibles)
				this.pantalla.addImagen(elemento.getPosicion().getX(), 
						elemento.getPosicion().getY(), 
						elemento.getRuta());
		}
		/*
		 * Bucle con indice
		 */
		/*for (int i = 0; i < this.topos.size(); i++) {
			if (!this.isVisible(this.topos.get(i).getPosicion()))
				this.pantalla.addImagen(this.topos.get(i).getPosicion().getX(), 
						this.topos.get(i).getPosicion().getY(), 
						this.topos.get(i).getRuta());
		}*/
		
		this.pantalla.addImagen(this.objetivo.getX(), this.objetivo.getY(), "imagenes/objetivo.png");
		
		this.pantalla.setBarraEstado("Tiempo restante: " + this.partida.getTiempoRestante()
									+ " | Disparos restantes: " + this.partida.getDisparosRestantes()
									+ " | Puntuacion: " + this.partida.getPuntos());
		
		this.pantalla.dibujar();
	}
	
	/**
	 * Actualiza las mecanicas del juego
	 * 
	 */
	public void actualizar() {
		for (int i = 0; i < this.alto; i++) {
			for (int j = 0; j < this.ancho; j++) {
				this.paneles[j][i].actualizar();
			}
		}
		
		LinkedList<Posicion> remove = new LinkedList<Posicion>();
		for (Elemento elemento : this.elementos.values()) {
			Posicion antigua = elemento.getPosicion();
			if (elemento.isActive()) elemento.actuar();
			if (!antigua.equals(elemento.getPosicion())) {
				remove.add(antigua);
			}
		}
		for (Posicion p : remove) {
			Elemento e = this.elementos.get(p);
			this.elementos.remove(p);
			this.elementos.put(e.getPosicion(), e);
		}
		/*
		 * Bucle con indice 
		 */
		/*for (int i = 0; i < this.topos.size(); i++)
			this.topos.get(i).actuar();
		*/
	}
	
	/**
	 * Dispara
	 * si es un elemento, lo elimina
	 * si en panel, lo oculta
	 * 
	 */
	public void disparar() {
		if (this.partida == null) return;
		else this.partida.disminuirMunicion(1);
		if (!this.isVisible(objetivo)) {
			this.paneles[objetivo.getX()][objetivo.getY()].golpear();
		}
		else if (this.thereElemento(objetivo)) {
			Elemento golpeado = this.getElemento(objetivo);
			golpeado.actualizar(this.partida);
			golpeado.setEscenario(null);
			this.elementos.remove(objetivo);
		}
	}
	
	/**
	 * Inicia la partida y ejecuta el bucle principal del juego
	 * 
	 * @param gameSeconds tiempo del juego en segundos
	 * @param initialShots municion inicial del jugador
	 */
	public void iniciarPartida(int gameSeconds, int initialShots) {
		this.run = true;
		this.puntosTotales = this.getPuntosTotales();
		this.partida = new GestionJuego(gameSeconds, initialShots);
		this.pantalla = new Pantalla(this.ancho, this.alto, 64, Color.BLUE);
		this.refrescarPantalla();
		
		while(!this.partida.isGameOver() && this.run) {
			
			if (this.pantalla.hayTecla()) {
				switch(this.pantalla.leerTecla()) {
					case "i":
						this.desplazarObjetivo(Direccion.ARRIBA);
						break;
						
					case "j":
						this.desplazarObjetivo(Direccion.IZQUIERDA);
						break;
						
					case "k":
						this.desplazarObjetivo(Direccion.ABAJO);
						break;
						
					case "l":
						this.desplazarObjetivo(Direccion.DERECHA);
						break;
						
					case "d":
						this.disparar();
						break;
						
					case "g" :
						this.visibles = !this.visibles;
						break;
					default:
						break;
				}
			}
			
			boolean fin = true;
			for (Elemento e : this.getElementos())
				if (e.isObjective()) fin = false;
			
			this.actualizar();		
			this.refrescarPantalla();
			if (fin) {
				this.win();
			}
			Alarma.dormir(ZOMBIE);
		}
		if (this.partida.isGameOver())
			this.pantalla.setBarraEstado("GAME OVER con " + this.partida.getPuntos() + " puntos.");
		this.pantalla.dibujar();
	}
	
	/**
	 * Has Ganado
	 * 
	 */
	public void win () {
		this.pantalla.setBarraEstado("Has ganado con " + this.partida.getPuntos() + " puntos de " + this.puntosTotales + " puntos.");
		this.run = false;
	}
	
	/**
	 * Obtiene la partida actual
	 * 
	 */
	public GestionJuego getPartida() {
		return this.partida;
	}
	
	/**
	 * Obtiene la lista de paneles
	 * 
	 */
	public LinkedList<PanelBasico> getPaneles() {
		LinkedList<PanelBasico> lista = new LinkedList<PanelBasico>();
		for(int j = 0; j < this.alto; j++) {
			for(int i = 0; i < this.ancho; i++) {
				lista.add(this.paneles[i][j].clone());
			}
		}
		return lista;
	}
	
	/**
	 * Obtiene un panel en la posicion dada
	 * 
	 * @param p posicion del panel a obtener
	 */
	public PanelBasico getPanel(Posicion p) {
		return this.paneles[p.getX()][p.getY()].clone();
	}
	
	/**
	 * Pone un panel en la posicion indicada por este
	 * 
	 * @param panel panel a colocar en el escenario
	 */
	public void setPanel(PanelBasico panel) {
		if(this.isValidPosition(panel.getPosicion())) {
			this.paneles[panel.getPosicion().getX()][panel.getPosicion().getY()] = panel.clone();
		}
	}
	
	/**
	 * Obtener la visibilidad del panel en la posición dada
	 * 
	 * @param p posicion a comprobar la visibilidad
	 */
	public boolean isVisible(Posicion p) {
		if (this.isValidPosition(p))
			return !this.getPanel(p).getVisible();
		else return false;
	}
	
	/**
	 * Comprueba si la posición dada está dentro del escenario
	 * 
	 * @param p posicion a comprobar
	 */
	public boolean isValidPosition(Posicion p) {
		if ((p.getX() >= 0 && p.getX() < this.ancho) && (p.getY() >= 0 && p.getY() < this.alto)) return true;
		return false;
	}
	
	/**
	 * Desplaza el objetivo en la dirección indicada
	 * 
	 * @param dir dirección del desplazamiento direccional
	 */
	public void desplazarObjetivo(Direccion dir) {
		if (isValidPosition(this.objetivo.getVecina(dir))) objetivo.desplazar(dir);	
	}
	
	/**
	 * Obtener el alto del escenario
	 * 
	 */
	public int getAlto() {
		return this.alto;
	}
	
	/**
	 * Obtener el ancho del escenario
	 * 
	 */
	public int getAncho() {
		return this.ancho;
	}
	
	/**
	 * Obtener la posición del Objetivo
	 * 
	 */
	public Posicion getObjetivo() {
		Posicion p = new Posicion(this.objetivo);
		return p;
	}
	
	/**
	 * Comprueba si hay un elemento en la posición dada
	 * 
	 * @param p posición a comprobar si hay un elemento
	 */
	public boolean thereElemento (Posicion p) {
		if (this.getElemento(p) != null) return true;
		return false;
	}
	
	/**
	 * Obtiene el numero de puntos totales
	 * 
	 */
	public int getPuntosTotales () {
		int puntos = 0;
		for (Map.Entry<Posicion, Elemento> it : this.elementos.entrySet()) {
			if (it.getValue().getPuntos() > 0) 
				puntos += it.getValue().getPuntos();
		}
		return puntos;
	}
	
	/**
	 * Devuelve el elementos en la posición dada
	 * 
	 * @param p posición del elemento
	 */
	public Elemento getElemento (Posicion p) {
		for (Elemento elemento : this.elementos.values()) {
			if (elemento.getPosicion().equals(p)) 
				return elemento;
		}
		/*
		 * Bucle con indice 
		 */
		/*for (int i = 0; i < this.topos.size(); i++) {
			if (topos.get(i).getPosicion().isEquals(p)) {
				return topos.get(i);
			}
		}*/
		return null;
	}
	
	public Collection<Elemento> getElementos() {
		return Collections.unmodifiableCollection(this.elementos.values());
	}
	
	/**
	 * Añade un elemento al escenario
	 * 
	 * @param inElementos elementos a añadir
	 */
	public LinkedList<Elemento> addElementos (Elemento...inElementos) {
		LinkedList<Elemento> t = new LinkedList<Elemento>();
		for (Elemento elemento : inElementos) {
			if (!thereElemento(elemento.getPosicion()) && this.isValidPosition(elemento.getPosicion())) {
				elemento.setEscenario(this);
				this.elementos.put(elemento.getPosicion(), elemento);
			}
			else {
				t.add(elemento);
			}
		}
		/*
		 * Bucle for con Indice
		 */
		/*for (int i = 0; i < inTopos.length; i++) {
			if (!thereTopo(inTopos[i].getPosicion())) { 
				inTopos[i].setEscenario(this);
				this.topos.add(inTopos[i]);
			}
			else {
				t.add(inTopos[i]);
			}
		}*/
		return t;
	}
	
}
