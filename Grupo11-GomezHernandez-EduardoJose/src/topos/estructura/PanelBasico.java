package topos.estructura;

/**
 * Declaracion del panel basico.
 * 
 * @author Eduardo Jose y Diego Alonso. 
 * 
 */
public class PanelBasico implements Cloneable {

	private Posicion posicion;
	private boolean visible;
	private long hideTime;
	private static final int HIDE_TIME = 5000;
	
	
	/**
	 * Constructor
	 * 
	 * @param x Posicion x en el tablero
	 * @param y Posicion y en el tablero
	 * @param visible estado inicial de visibilidad
	 */
	public PanelBasico(int x, int y, boolean visible) {
		this.posicion = new Posicion(x,y);
		this.visible = visible;
		this.hideTime = 0;
	}
	
	/**
	 * Constructor
	 * 
	 * @param x Posicion x en el tablero
	 * @param y Posicion y en el tablero
	 */
	public PanelBasico(int x, int y) {
		this(x,y, true);
	}
	
	/**
	 * Constructor por copia
	 * 
	 */
	public PanelBasico(PanelBasico copy) {
		this(copy.posicion.getX(),copy.posicion.getY(), copy.visible);
	}
	
	/**
	 * Obtiene la imagen asociada al panel
	 * 
	 */
	public String getRuta() {
		return "imagenes/panel-basico.gif";
	}
	
	/**
	 * Oculta el panel
	 * 
	 */
	public void golpear() {
		this.visible = false;
		this.hideTime = System.currentTimeMillis();
	}
	
	/**
	 * Cambia el estado de visibilidad del panel
	 * 
	 */
	protected void setVisible (boolean visible) {
		this.visible = visible;
	}
	
	/**
	 * Muestra el panel cuando haya pasado cierto tiempo
	 * 
	 */
	public void actualizar() {
		if (System.currentTimeMillis() - this.hideTime >= HIDE_TIME)
			this.visible = true;
	}
	
	/**
	 * Metodo de consulta para obtener la posicion.
	 * 
	 * @return Devuelve una copia de la posicion actual.
	 */
	public Posicion getPosicion() {
		Posicion posicion2 = new Posicion(this.posicion);
		return posicion2;
	}
	
	/**
	 * Metodo de consulta para saber si esta visible.
	 * 
	 * @return Devuelve 
	 */
	public boolean getVisible() {
		return this.visible;
	}
	
	/**
	 * Metodo para establecer la posicion del panel.
	 * 
	 * @param posicion La posicion con la nueva posicion.
	 */
	public void setPosicion(Posicion posicion) {
		Posicion pos = new Posicion(posicion);
		this.posicion = pos;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (hideTime ^ (hideTime >>> 32));
		result = prime * result + ((posicion == null) ? 0 : posicion.hashCode());
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PanelBasico other = (PanelBasico) obj;
		if (hideTime != other.hideTime)
			return false;
		if (posicion == null) {
			if (other.posicion != null)
				return false;
		} else if (!posicion.equals(other.posicion))
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PanelBasico [posicion=" + posicion + ", visible=" + visible + ", hideTime=" + hideTime + "]";
	}
	
	@Override
	public PanelBasico clone() {
		return new PanelBasico(this);
	}
	
}
