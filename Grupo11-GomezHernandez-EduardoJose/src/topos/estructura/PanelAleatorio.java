package topos.estructura;

import java.util.Random;

/**
 * Representa el tipo de panel aleatorio en el juego.
 * Un panel aleatorio, cuyo tiempo de retorno al estado
 * original es aleatorio.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */

public class PanelAleatorio extends PanelBasico {
	
	/**
	 * Constructor
	 * 
	 * @param x Posicion x en el tablero
	 * @param y Posicion y en el tablero
	 * @param visible estado inicial de visibilidad
	 */
	public PanelAleatorio(int x, int y, boolean visible) {
		super(x,y,visible);
	}
	
	/**
	 * Constructor
	 * 
	 * @param x Posicion x en el tablero
	 * @param y Posicion y en el tablero
	 */
	public PanelAleatorio(int x, int y) {
		super(x,y);
	}
	
	public PanelAleatorio (PanelAleatorio copy) {
		this(copy.getPosicion().getX(), copy.getPosicion().getY(), copy.getVisible());
	}
	
	/**
	 * Golpeamos solo el 50% de la veces
	 * 
	 */
	@Override
	public void golpear() {
		Random rand = new Random();
		if (rand.nextInt(2) == 0) {
			super.golpear();
		}
	}	
	
	/**
	 * Obtenemos la imagen asociada al panel
	 * 
	 */
	@Override
	public String getRuta() {
		return "imagenes/panel-aleatorio.png";
	}

	@Override
	public String toString() {
		return "PanelAleatorio []";
	}
	
	@Override
	public PanelAleatorio clone() {
		return new PanelAleatorio(this);
	}
	
}
