package topos.elementos;
import java.util.LinkedList;
import java.util.Random;

import topos.estructura.Direccion;
import topos.estructura.Posicion;
import topos.juego.GestionJuego;

/**
 * Representa al topo en el juego.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class TopoListo extends Elemento {
	
	public static final int PUNTOS = 3;
		
	/**
	 * Declaracion de un topo.
	 * 
	 * @param x Coordenada x en la que se situa el topo.
	 * @param y Coordenada y en la que se situa el topo.
	 */
	public TopoListo(int x, int y) {
		super(x,y);
	}
	
	/**
	 * Obtiene la imagen asociada al topo
	 * 
	 */
	@Override
	public String getRuta() {
		return "imagenes/topo-listo.png";
	}
	
	@Override
	protected boolean canMove () {
		return !this.getEscenario().isVisible(this.getPosicion());
	}
	
	/**
	 * Define la IA del topo
	 * 
	 */
	protected Direccion ia () {
		LinkedList<Direccion> pos = new LinkedList<Direccion>();
		for (Direccion dir : Direccion.values()) {
			Posicion p = this.getPosicion().getVecina(dir);
			if (this.getEscenario().isValidPosition(p) && !this.getEscenario().isVisible(p)) {
				pos.add(dir);
			}
		}
		if (pos.isEmpty()) {
			return Direccion.getRandom();
		}
		else {
			Random rand = new Random();
			return pos.get(rand.nextInt(pos.size()));
		}
	}
	
	/**
	 * Obtiene si es un elemento activo o pasivo
	 * 
	 */
	@Override
	public boolean isActive() {
		return true;
	}
	
	/**
	 * Comprobar si es un objetivo para el jugador
	 * 
	 */
	@Override
	public boolean isObjective () {
		return true;
	}
	
	/**
	 * Aumenta los puntos cada vez que es golpeado
	 * 
	 */
	@Override
	public void actualizar(GestionJuego gestionJuego) {
		gestionJuego.aumentarPuntos(PUNTOS);
	}
	
	/**
	 * Obtiene el numero de puntos que ofrece el elemento al ser golpeado
	 * 
	 */
	@Override
	public int getPuntos() {
		return PUNTOS;
	}
}
