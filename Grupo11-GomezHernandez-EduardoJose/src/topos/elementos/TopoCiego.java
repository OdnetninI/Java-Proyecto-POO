package topos.elementos;
import topos.estructura.Direccion;
import topos.juego.GestionJuego;

/**
 * Representa al topo en el juego.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class TopoCiego extends Elemento {
	
	public final static int PUNTOS = 2;
	
	private Direccion ultimaDireccion = null;
	
	/**
	 * Declaracion de un topo.
	 * 
	 * @param x Coordenada x en la que se situa el topo.
	 * @param y Coordenada y en la que se situa el topo.
	 */
	public TopoCiego(int x, int y) {
		super(x,y);
	}
	
	/**
	 * Comprobar si es un objetivo para el jugador
	 * 
	 */
	@Override
	public boolean isObjective () {
		return true;
	}
	
	/**
	 * Obtiene la imagen asociada al topo
	 * 
	 */
	@Override
	public String getRuta() {
		return "imagenes/topo-ciego.png";
	}
	
	@Override
	protected boolean canMove () {
		return true;
	}
	
	/**
	 * Define la IA del topo
	 * 
	 */
	protected Direccion ia () {
		if (this.ultimaDireccion == null) {
			this.ultimaDireccion = Direccion.getRandom();
			return this.ultimaDireccion;
		}
		else {
			Direccion moverse = Direccion.getRandom();
			boolean inValid = true;
			while (inValid) {
				if (this.ultimaDireccion == Direccion.ARRIBA){
					if (moverse != Direccion.ARRIBA) {
						this.ultimaDireccion = moverse;
						inValid = false;
					}
					else moverse = Direccion.getRandom();
				}
				else if (this.ultimaDireccion == Direccion.ABAJO){
					if (moverse != Direccion.ABAJO) {
						this.ultimaDireccion = moverse;
						inValid = false;
					}
					else moverse = Direccion.getRandom();
				}
				else if (this.ultimaDireccion == Direccion.IZQUIERDA){
					if (moverse != Direccion.IZQUIERDA) {
						this.ultimaDireccion = moverse;
						inValid = false;
					}
					else moverse = Direccion.getRandom();
				}
				else if (this.ultimaDireccion == Direccion.DERECHA){
					if (moverse != Direccion.DERECHA) {
						this.ultimaDireccion = moverse;
						inValid = false;
					}
					else moverse = Direccion.getRandom();
				}
			}
			return moverse;
		}
	}
	
	/**
	 * Obtiene si es un elemento activo o pasivo
	 * 
	 */
	@Override
	public boolean isActive() {
		return true;
	}
	
	/**
	 * Aumenta los puntos cada vez que es golpeado
	 * 
	 */
	@Override
	public void actualizar(GestionJuego gestionJuego) {
		gestionJuego.aumentarPuntos(PUNTOS);
	}
	
	/**
	 * Obtiene el numero de puntos que ofrece el elemento al ser golpeado
	 * 
	 */
	@Override
	public int getPuntos() {
		return PUNTOS;
	}
	
}
