package topos.elementos;
import topos.estructura.Direccion;
import topos.estructura.Escenario;
import topos.estructura.Posicion;
import topos.juego.GestionJuego;

/**
 * Representa al topo en el juego.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public abstract class Elemento {
	private Posicion posicion;
	private Escenario escenario;
	private long tiempoUltimoMovimiento = 0;
	private static final int TIEMPO_ESPERA = 2000;
	
	/**
	 * CTR default
	 * 
	 */
	public Elemento () {
		this(0,0);
	}
	
	/**
	 * CTR dadas unas coordenadas
	 * 
	 */
	public Elemento(int x, int y) {
		if (x <= 0 || y <= 0) throw new IllegalArgumentException("Las posiciones x e y deben ser positivas y mayores que 0");
		this.posicion = new Posicion(x,y);
	}
	
	/**
	 * Comprobar si es un objetivo para el jugador
	 * 
	 */
	public boolean isObjective () {
		return false;
	}
	
	/**
	 * COpy CTR
	 * 
	 */
	public Elemento (Posicion posicion) {
		this(posicion.getX(), posicion.getY());
	}
	
	/**
	 * Obtiene si es un elemento activo o pasivo
	 * 
	 */
	public boolean isActive() {
		return false;
	}
	
	/**
	 * Aumenta los puntos cada vez que es golpeado
	 * 
	 */
	public abstract void actualizar(GestionJuego gestionJuego);
	
	/**
	 * Obtiene el numero de puntos que ofrece el elemento al ser golpeado
	 * 
	 */
	public int getPuntos() {
		return 0;
	}
	
	/**
	 * Obtiene la imagen del elemento
	 * 
	 */
	public abstract String getRuta();
	
	/**
	 * Asigna un escenario al elemento
	 * 
	 */
	public void setEscenario (Escenario escenario) {
		this.escenario = escenario;
	}
	
	/**
	 * Obtiene el escenario asignado al elemento
	 * 
	 */
	public Escenario getEscenario () {
		return this.escenario;
	}
	
	/**
	 * Obtiene la posicion del elemento
	 * 
	 */
	public Posicion getPosicion() {
		Posicion posicion = new Posicion(this.posicion);
		return posicion;
	}
	
	/**
	 * Asigna una posicion al elemento
	 * 
	 */
	public void setPosicion(Posicion pos) {
		Posicion p = new Posicion(pos);
		this.posicion = p;
	}
	
	/**
	 * Especifica si el elemento se puede mover o no
	 * 
	 */
	protected boolean canMove () {return false;}
	
	/**
	 * Define el comportamiento de los elementos
	 * 
	 */
	public void actuar() {
		if (this.escenario != null && this.isActive() && this.canMove()) {
			if (System.currentTimeMillis() - this.tiempoUltimoMovimiento >= TIEMPO_ESPERA) {
				Direccion dir = this.ia();
				if (this.escenario.isValidPosition(this.posicion.getVecina(dir)) && !this.escenario.thereElemento(this.posicion.getVecina(dir)))
				{
					this.posicion.desplazar(dir);
					this.tiempoUltimoMovimiento = System.currentTimeMillis();
				}
			}
		}
	}
	
	/**
	 * Obtiene la direccion del elemento obtenida por la IA
	 * 
	 */
	protected Direccion ia () {return null;}
	
	/**
	 * Desplaza el elemento en la dirección asignada.
	 * 
	 */
	public void desplazar(Direccion direccion) {
		if (this.escenario != null && this.escenario.isValidPosition(this.posicion.getVecina(direccion)))
			this.posicion.desplazar(direccion);
	}
	
}
