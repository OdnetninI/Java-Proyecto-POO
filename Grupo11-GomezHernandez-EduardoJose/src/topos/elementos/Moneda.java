package topos.elementos;
import topos.juego.GestionJuego;

/**
 * Representa al topo en el juego.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class Moneda extends Elemento {
	private final int valor;
	
	/**
	 * Declaracion de un topo.
	 * 
	 * @param x Coordenada x en la que se situa el topo.
	 * @param y Coordenada y en la que se situa el topo.
	 */
	public Moneda(int x, int y, int valor) {
		super(x,y);
		this.valor = valor;
	}
	
	/**
	 * Obtiene la imagen asociada al topo
	 * 
	 */
	@Override
	public String getRuta() {
		return "imagenes/monedas.png";
	}
	
	/**
	 * Obtiene si es un elemento activo o pasivo
	 * 
	 */
	@Override
	public boolean isActive() {
		return false;
	}
	
	/**
	 * Comprobar si es un objetivo para el jugador
	 * 
	 */
	@Override
	public boolean isObjective () {
		return true;
	}
	
	/**
	 * Aumenta los puntos cada vez que es golpeado
	 * 
	 */
	@Override
	public void actualizar(GestionJuego gestionJuego) {
		gestionJuego.aumentarPuntos(this.valor);
	}
	
	/**
	 * Obtiene el numero de puntos que ofrece el elemento al ser golpeado
	 * 
	 */
	@Override
	public int getPuntos() {
		return this.valor;
	}
}
