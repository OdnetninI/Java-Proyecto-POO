package topos.elementos;
import topos.estructura.Direccion;
import topos.estructura.Posicion;
import topos.juego.GestionJuego;

/**
 * Representa al topo en el juego.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class Rata extends Elemento {
	
	public static final int PUNTOS = 1;
	
	/**
	 * Declaracion de un topo.
	 * 
	 * @param x Coordenada x en la que se situa el topo.
	 * @param y Coordenada y en la que se situa el topo.
	 */
	public Rata(int x, int y) {
		super(x,y);
	}
	
	/**
	 * Obtiene la imagen asociada al topo
	 * 
	 */
	@Override
	public String getRuta() {
		return "imagenes/rata.png";
	}
	
	@Override
	protected boolean canMove () {
		return !this.getEscenario().isVisible(this.getPosicion());
	}
	
	/**
	 * Define la IA del topo
	 * 
	 */
	protected Direccion ia () {
		Posicion d = this.getPosicion().getVecina(Direccion.DERECHA); 
		Posicion a = this.getPosicion().getVecina(Direccion.ARRIBA); 
		Posicion i = this.getPosicion().getVecina(Direccion.IZQUIERDA); 
		Posicion b = this.getPosicion().getVecina(Direccion.ABAJO); 
		if (this.getEscenario().isValidPosition(d) && this.getEscenario().isVisible(d))
			return Direccion.DERECHA;
		else if (this.getEscenario().isValidPosition(a) && this.getEscenario().isVisible(a))
			return Direccion.ARRIBA;
		else if (this.getEscenario().isValidPosition(i) && this.getEscenario().isVisible(i))
			return Direccion.IZQUIERDA;
		else if (this.getEscenario().isValidPosition(b) && this.getEscenario().isVisible(b))
			return Direccion.ABAJO;
		else
			return Direccion.getRandom();
	}
	
	/**
	 * Obtiene si es un elemento activo o pasivo
	 * 
	 */
	@Override
	public boolean isActive() {
		return true;
	}
	
	/**
	 * Aumenta los puntos cada vez que es golpeado
	 * 
	 */
	@Override
	public void actualizar(GestionJuego gestionJuego) {
		gestionJuego.disminuirPuntos(PUNTOS);
	}
	
	/**
	 * Obtiene el numero de puntos que ofrece el elemento al ser golpeado
	 * 
	 */
	@Override
	public int getPuntos() {
		return -PUNTOS;
	}
	
}
