package topos.elementos;
import topos.estructura.Direccion;
import topos.juego.GestionJuego;

/**
 * Representa al topo en el juego.
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class TopoTorpe extends Elemento {
	
	public final static int PUNTOS = 1;
	
	/**
	 * Declaracion de un topo.
	 * 
	 * @param x Coordenada x en la que se situa el topo.
	 * @param y Coordenada y en la que se situa el topo.
	 */
	public TopoTorpe(int x, int y) {
		super(x,y);
	}
	
	/**
	 * Obtiene la imagen asociada al topo
	 * 
	 */
	@Override
	public String getRuta() {
		return "imagenes/topo-torpe.png";
	}
	
	@Override
	protected boolean canMove () {
		return !this.getEscenario().isVisible(this.getPosicion());
	}
	
	/**
	 * Define la IA del topo
	 * 
	 */
	protected Direccion ia () {
		return Direccion.getRandom();
	}
	
	@Override
	public boolean isActive() {
		return true;
	}
	
	/**
	 * Aumenta los puntos cada vez que es golpeado
	 * 
	 */
	@Override
	public void actualizar(GestionJuego gestionJuego) {
		gestionJuego.aumentarPuntos(PUNTOS);
	}
	
	/**
	 * Comprobar si es un objetivo para el jugador
	 * 
	 */
	@Override
	public boolean isObjective () {
		return true;
	}
	
	
	/**
	 * Obten el numero de puntos que ofrece este elemento
	 * 
	 */
	@Override
	public int getPuntos() {
		return PUNTOS;
	}
	
}
