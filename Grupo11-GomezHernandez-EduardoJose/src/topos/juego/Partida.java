package topos.juego;

import topos.elementos.*;
import topos.estructura.*;

/**
 * Partida actual del juego
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class Partida {
	/**
	 * Punto de inicio del juego
	 * 
	 */
	public static void main (String Args[]) {
		Escenario scene = new Escenario(10, 10);
		
		PanelResistente uno = new PanelResistente(5, 5, 3);
		PanelFlash dos = new PanelFlash(4, 4);
		PanelAleatorio thresh = new PanelAleatorio(3, 3);
		scene.setPanel(uno);
		scene.setPanel(dos);
		scene.setPanel(thresh);
		
		Elemento a = new Rata(1, 1);
		Elemento b = new TopoListo(2, 1);
		Elemento c = new TopoCiego(3, 1);
		Elemento d = new TopoTorpe(1, 8);
		Elemento e = new Moneda (2,7,65);
		Elemento f = new Municion (3, 5, 9999);
		Elemento g = new Reloj (4, 4, 1000);
		scene.addElementos(a,b,c,d,e,f,g);
		scene.iniciarPartida(60, 50);
	}
}
