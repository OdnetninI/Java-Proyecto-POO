package topos.juego;

/**
 * Gestiona los datos del juego
 * 
 * @author Eduardo Jose y Diego Alonso.
 *
 */
public class GestionJuego {
	private int puntos;
	private int disparosRestantes;
	private long inicio;
	private int tiempo;
	
	/**
	 * Constructor
	 * 
	 * @param tiempo tiempo maximo de partida
	 * @param shots numero inicial de disparos
	 */
	public GestionJuego(int tiempo, int shots) {
		this.tiempo = tiempo;
		this.disparosRestantes = shots;
		this.inicio = System.currentTimeMillis();
	}
	
	/**
	 * Obtener el numero de disparos restantes
	 * 
	 */
	public int getDisparosRestantes() {
		return this.disparosRestantes;
	}
	
	/**
	 * Obtener los puntos
	 * 
	 */
	public int getPuntos() {
		return this.puntos;
	}
	
	/**
	 * Aumenta la puntuacion del jugador en x
	 * 
	 * @param x numero de puntos a incrementar
	 */
	public void aumentarPuntos(int x) {
		this.puntos += x;
	}
	
	/**
	 * Disminuye la puntuacion del jugador en x
	 * 
	 * @param x numero de puntos a disminuir
	 */
	public void disminuirPuntos(int x) {
		this.puntos -= x;
	}
	
	/**
	 * Aumenta la municion del jugador en x
	 * 
	 * @param x numero de municion a incrementar
	 */
	public void aumentarMunicion (int x) {
		this.disparosRestantes += x;
	}
	
	/**
	 * Disminuye la municion del jugador en x
	 * 
	 * @param x numero de municion a disminuir
	 */
	public void disminuirMunicion (int x) {
		this.disparosRestantes -= x;
	}
	
	/**
	 * Obtener el tiempo que llevamos jugando en segundo
	 * 
	 */
	public int getTiempoPartida () {
		return (int) Math.ceil((System.currentTimeMillis() - this.inicio)/1000);
	}
	
	/**
	 * Obtener el tiempo que no queda de partida
	 * 
	 */
	public int getTiempoRestante() {
		return (this.tiempo - this.getTiempoPartida() < 0)? 0 : (this.tiempo - this.getTiempoPartida());
	}
	
	/**
	 * Aumenta el tiempo
	 * 
	 */
	public void aumentarTiempo (int x) {
		this.tiempo += x;
	}
	
	/**
	 * Obtener si el juego ha acabado
	 * 
	 */
	public boolean isGameOver () {
		if((this.getTiempoRestante() <= 0) || (this.disparosRestantes <= 0)) return true;
		return false;
	}
	
}
